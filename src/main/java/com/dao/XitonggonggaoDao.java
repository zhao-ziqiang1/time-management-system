package com.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.entity.XitonggonggaoEntity;
import com.entity.view.XitonggonggaoView;
import com.entity.vo.XitonggonggaoVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 系统公告
 * 
 * @author 
 * @email 
 * @date 2022-03-04 15:54:43
 */
public interface XitonggonggaoDao extends BaseMapper<XitonggonggaoEntity> {
	
	List<XitonggonggaoVO> selectListVO(@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);
	
	XitonggonggaoVO selectVO(@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);
	
	List<XitonggonggaoView> selectListView(@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);

	List<XitonggonggaoView> selectListView(Pagination page,@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);
	
	XitonggonggaoView selectView(@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);
	

}
