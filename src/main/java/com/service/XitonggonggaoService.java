package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.entity.XitonggonggaoEntity;
import com.entity.view.XitonggonggaoView;
import com.entity.vo.XitonggonggaoVO;
import com.utils.PageUtils;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 系统公告
 *
 * @author 
 * @email 
 * @date 2022-03-04 15:54:43
 */
public interface XitonggonggaoService extends IService<XitonggonggaoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<XitonggonggaoVO> selectListVO(Wrapper<XitonggonggaoEntity> wrapper);
   	
   	XitonggonggaoVO selectVO(@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);
   	
   	List<XitonggonggaoView> selectListView(Wrapper<XitonggonggaoEntity> wrapper);
   	
   	XitonggonggaoView selectView(@Param("ew") Wrapper<XitonggonggaoEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<XitonggonggaoEntity> wrapper);
   	

}

